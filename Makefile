APP=slave
all: $(APP)
JRUBYJAR="/home/foilo/.rvm/rubies/jruby-1.7.19/lib/jruby.jar"
JAVA_JAR="jar"
JAVA_FUNC="org.drftpd.slave.Slave"

all: $(APP)

makejar:
	@cp $(JRUBYJAR) $(APP).jar
	@ls -lah $(APP).jar
	@echo "..."
	@$(JAVA_JAR) -uf $(APP).jar -C classes .
	@ls -lah $(APP).jar
	@echo "Packing done ..."


slave: makejar
	@@$(JAVA_JAR) -ufe $(APP).jar $(JAVA_FUNC)
	@ls -lah $(APP).jar
	@echo "Done..."
	
clean: 
	rm -fR $(APP)*.jar *.tmp




