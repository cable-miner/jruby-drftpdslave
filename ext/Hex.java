package org.drftpd.slave;

public class Hex
{
   /**
    * Constructeur prive : interdit l'instanciation de cette classe.
    */
   private Hex() {}

   /**
    * Decode la sequence de caracteres hexadecimaux specifiee sous forme de tableau d'octets.
    *
    * @param data Donnees a decoder.
    * @return Donnees decodees.
    *
    * @throws IllegalArgumentException <var>data</var> contient un nombre impair de caracteres ou des caracteres invalides.
    */
   public static byte[] decode(String data)
   {
      // Verification des arguments
      int length=data.length();
      if((length%2)!=0) throw new IllegalArgumentException("Odd number of characters.");

      // 2 caracteres hexadecimaux sont utilises pour representer un octet
      try
      {
         byte[] bytes=new byte[length/2];
         for(int i=0, j=0; i<length; i=i+2) bytes[j++]=(byte) Integer.parseInt(data.substring(i, i+2), 16);
         return bytes;
      }
     
      catch(NumberFormatException e)
      {
         throw new IllegalArgumentException("Illegal hexadecimal character.", e);
      }
   }
   
   /**
    * Code les donnees specifiees sous forme de chaine composee de caracteres hexadecimaux.
    *
    * @param data Donnees a coder.
    * @return Donnees codees sous forme de chaine hexadecimale.
    */
   public static String encode(byte[] data)
   {
      StringBuilder builder=new StringBuilder();
      
      for(int i=0; i<data.length;i++)
      //for(byte dataByte: data)
      {
         // Un octet est represente par 2 caracteres hexadecimaux
         //builder.append(Integer.toHexString((dataByte & 0xF0)>>4));
         //builder.append(Integer.toHexString(dataByte & 0x0F));
         builder.append(Integer.toHexString((data[i] & 0xF0)>>4));
         builder.append(Integer.toHexString(data[i] & 0x0F));
      }
      
      return builder.toString();
   }
}
