package org.drftpd.slave.async;

import java.util.ArrayList;

public class AsyncResponseRootFree extends AsyncResponse {
    ArrayList _roots;

    public AsyncResponseRootFree(String index, ArrayList roots) {
        super(index);
        _roots = roots;
    }

    public ArrayList getRoots() {
        return _roots;
    }

    public String toString() {
        return getClass().getName() + "[roots=" + _roots + "]";
    }
}