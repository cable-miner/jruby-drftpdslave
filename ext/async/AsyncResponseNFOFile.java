package org.drftpd.slave.async;

import net.k2rmods.plugins.irc.imdb.NFOFile;

public class AsyncResponseNFOFile extends AsyncResponse {
    
	private static final long serialVersionUID = 1L;
	NFOFile _nfo;

    public AsyncResponseNFOFile(String index, NFOFile nfo) {
        super(index);
        _nfo = nfo;
    }

    public NFOFile getNFO() {
        return _nfo;
    }

}