package org.drftpd.slave.async;

public class AsyncResponseBannedTransfer extends AsyncResponse {

	private boolean _banned;
	private String _ipbanned;
	private String _direction;
	private String _username;
	private String _userip;
	private String _slaveip;
	private String _path;
	
    public AsyncResponseBannedTransfer(String ipBanned, String direction, String user,
    		String userip, String slaveip, String path, boolean banned) {
        super("ipbanned");
        
        if (ipBanned == null) {
            throw new IllegalArgumentException("ipbanned cannot be null");
        }

        _ipbanned = ipBanned;
        _direction = direction;
        _username = user;
        _userip = userip;
        _slaveip = slaveip;
        _path = path;
        _banned = banned;
    }

    public String getIpbanned() {
        return _ipbanned;
    }
    
    public String getDirection() {
    	return _direction;
    }
    
    public String getUserName() {
    	return _username;
    }
    
    public String getUserIP() {
    	return _userip;
    }
    
    public String getSlaveIP() {
   	return _slaveip;
    }
    
    public String getPath() {
    	return _path;
    }
    
    public boolean isReallyBan() {
    	return _banned;
    }

    public String toString() {
        return getClass().getName() + "[ipBanned=" + getIpbanned() + "]";
    }
}