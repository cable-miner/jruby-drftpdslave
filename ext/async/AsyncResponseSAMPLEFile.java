package org.drftpd.slave.async;

import net.k2rmods.plugins.irc.sample.SampleInfos;

//K2RSAMPLE


public class AsyncResponseSAMPLEFile extends AsyncResponse {
    
	SampleInfos _sample;

    public AsyncResponseSAMPLEFile(String index, SampleInfos sample) {
        super(index);
        _sample = sample;
    }

    public SampleInfos getSAMPLE() {
        return _sample;
    }

}

//K2RSAMPLE