package org.drftpd.slave.async;

import java.net.InetAddress;

public class AsyncResponseRemoteAddress extends AsyncResponse {
    InetAddress _remoteAddress;

    public AsyncResponseRemoteAddress(String index, InetAddress remoteAddress) {
        super(index);
        _remoteAddress = remoteAddress;
    }

    public InetAddress getRemoteAddress() {
        return _remoteAddress;
    }

    public String toString() {
        return getClass().getName() + "[remoteAddress=" + _remoteAddress + "]";
    }
}